

/*
//One could concievably put this into a header file
*/

// CPU Clock Speed (1 MHz)
#define F_CPU 1000000UL

// Headers
#include <avr/io.h>
#include <util/delay.h>

// Global Variables
// (Mostly shift_register commands)
#define STR     0
#define DAT     1
#define CLK     2

// Function prototypes
void print7seg_sr(int);
void shiftin(char[]);

int main(void)
{
    // Main. Nah mean
    
    DDRB =  0x0F;   // 4 outputs PORTB[3..0]
    PORTB = 0x00;   // Everything but strobe off
    
    while(1)
    {
        for(int i = 0; i <= 9; i++)
        {
            print7seg_sr(i);      // Load

        }
    }
}

void print7seg_sr(int n)
{
    switch(n)
    {
        case 0:
        // abcdef = 1111 1100 = 0xFC
        shiftin("11111100");
        break;
        
        case 1:
        // bc = 0110 0000
        shiftin("01100000");
        break;
        
        case 2:
        // abdeg = 1101 1010
        shiftin("11011010");
        break;
        
        case 3:
        // abcdg = 1111 0010
        shiftin("11110010");
        break;
        
        case 4:
        // bcfg = 0110 0110 
        shiftin("01000110");
        break;
        
        case 5:
        // acdfg = 1011 0110
        shiftin("10110110");
        break;
        
        case 6:
        // cdefg = 0011 1110
        shiftin("00111110");
        break;
        
        case 7:
        // abc = 1110 0000
        shiftin("11100000");
        break;

        case 8:
        // abcdefg = 1111 1110
        shiftin("11111110");
        break;

        case 9:
        // abcfg = 1110 0110
        shiftin("11100110");
        break;
        
        default:
        // Error condition: prints "E."
        // adefg. = 10011111
        shiftin("10011111");
        break;
    }
}

// Very important that word[] is always length 8
void shiftin(char word[])
{
    for(int i = 0; i <=7; i++)
    {
        // Data Pin Setup
        if (word[i] == '0')
        {
            PORTB = 0x00;
        } else if (word[i] == '1') 
        {
            PORTB = 0x02;
        }
        
        // Strobe Now or Strobe Later?
        // Stobe Later

        // Clocking
        PORTB |= (1 << CLK);
        _delay_ms(1);
        PORTB &= ~(1 << CLK);
        _delay_ms(1);
    }
    PORTB |= 1 << STR; // Strobe and wait
    _delay_ms(100);
}
